import 'package:auto_route/auto_route.dart';
import 'package:managing_movies/screens/crud/add.dart';
import 'package:managing_movies/screens/home/home.dart';



export 'router.gr.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      path: '/',
      page: HomePage,
    
    ),
    AutoRoute(page: AddMovies),

    RedirectRoute(path: '*', redirectTo: '/'),
  ],
)
class $AppRouter {}
