// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cons.add.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ConsAdd on _ConsAdd, Store {
  late final _$valueAtom = Atom(name: '_ConsAdd.value', context: context);

  @override
  bool get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(bool value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  late final _$value2Atom = Atom(name: '_ConsAdd.value2', context: context);

  @override
  bool get value2 {
    _$value2Atom.reportRead();
    return super.value2;
  }

  @override
  set value2(bool value) {
    _$value2Atom.reportWrite(value, super.value2, () {
      super.value2 = value;
    });
  }

  late final _$value3Atom = Atom(name: '_ConsAdd.value3', context: context);

  @override
  bool get value3 {
    _$value3Atom.reportRead();
    return super.value3;
  }

  @override
  set value3(bool value) {
    _$value3Atom.reportWrite(value, super.value3, () {
      super.value3 = value;
    });
  }

  late final _$value4Atom = Atom(name: '_ConsAdd.value4', context: context);

  @override
  bool get value4 {
    _$value4Atom.reportRead();
    return super.value4;
  }

  @override
  set value4(bool value) {
    _$value4Atom.reportWrite(value, super.value4, () {
      super.value4 = value;
    });
  }

  late final _$_ConsAddActionController =
      ActionController(name: '_ConsAdd', context: context);

  @override
  void chaenge() {
    final _$actionInfo =
        _$_ConsAddActionController.startAction(name: '_ConsAdd.chaenge');
    try {
      return super.chaenge();
    } finally {
      _$_ConsAddActionController.endAction(_$actionInfo);
    }
  }

  @override
  void chaenge2() {
    final _$actionInfo =
        _$_ConsAddActionController.startAction(name: '_ConsAdd.chaenge2');
    try {
      return super.chaenge2();
    } finally {
      _$_ConsAddActionController.endAction(_$actionInfo);
    }
  }

  @override
  void chaenge3() {
    final _$actionInfo =
        _$_ConsAddActionController.startAction(name: '_ConsAdd.chaenge3');
    try {
      return super.chaenge3();
    } finally {
      _$_ConsAddActionController.endAction(_$actionInfo);
    }
  }

  @override
  void chaenge4() {
    final _$actionInfo =
        _$_ConsAddActionController.startAction(name: '_ConsAdd.chaenge4');
    try {
      return super.chaenge4();
    } finally {
      _$_ConsAddActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value},
value2: ${value2},
value3: ${value3},
value4: ${value4}
    ''';
  }
}
