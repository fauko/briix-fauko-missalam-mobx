import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'controller/cons.add.dart';

class AddMovies extends StatefulWidget {
  const AddMovies({Key? key}) : super(key: key);

  @override
  State<AddMovies> createState() => _AddMoviesState();
}

class _AddMoviesState extends State<AddMovies> {
  @override
  Widget build(BuildContext context) {
    final ConsAdd change = ConsAdd();

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size,
        child: Container(
          padding: const EdgeInsets.only(top: 20),
          height: 100,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(
            color: Colors.blueAccent,
          ),
          child: Expanded(
            child: Row(
              children: [
                // IconButton(
                //   onPressed: () {
                //     Navigator.pop(context);
                //   },
                //   icon: const Icon(Icons.arrow_back),
                //   color: Colors.white,
                // ),
                const Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.delete_forever),
                  color: Colors.white,
                  iconSize: 30,
                ),
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.save),
                  color: Colors.white,
                  iconSize: 30,
                )
              ],
            ),
          ),
        ),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.height,
        child: Expanded(
          child: SingleChildScrollView(
              child: Form(
                  child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    width: 1,
                    color: Colors.blue[50]!,
                  ),
                  boxShadow: const [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.07999999821186066),
                        offset: Offset(2, 2),
                        blurRadius: 3)
                  ],
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
                child: Expanded(
                  child: Column(
                    children: [
                      Container(
                          margin: const EdgeInsets.only(right: 300),
                          child: const Text(
                            "Title :",
                            style: TextStyle(fontSize: 16),
                          )),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            hintText: 'Masukan Title',
                            hintStyle:
                                TextStyle(overflow: TextOverflow.ellipsis),
                            isDense: true,
                            contentPadding: EdgeInsets.zero,
                            border: InputBorder.none,
                          ),
                          style: const TextStyle(color: Colors.black),
                          // onChanged: (value) =>
                          //     faqSearchQuestionController.keyword.value = value,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    width: 1,
                    color: Colors.blue[50]!,
                  ),
                  boxShadow: const [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.07999999821186066),
                        offset: Offset(2, 2),
                        blurRadius: 3)
                  ],
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
                child: Expanded(
                  child: Column(
                    children: [
                      Container(
                          margin: const EdgeInsets.only(right: 287, left: 9),
                          child: const Text(
                            "Director :",
                            style: TextStyle(fontSize: 16),
                          )),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            hintText: 'Masukan Director',
                            hintStyle:
                                TextStyle(overflow: TextOverflow.ellipsis),
                            isDense: true,
                            contentPadding: EdgeInsets.zero,
                            border: InputBorder.none,
                          ),
                          style: const TextStyle(color: Colors.black),
                          // onChanged: (value) =>
                          //     faqSearchQuestionController.keyword.value = value,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    width: 1,
                    color: Colors.blue[50]!,
                  ),
                  boxShadow: const [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.07999999821186066),
                        offset: Offset(2, 2),
                        blurRadius: 3)
                  ],
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
                child: Expanded(
                  child: Column(
                    children: [
                      Container(
                          margin: const EdgeInsets.only(right: 275),
                          child: const Text(
                            "Sunmary :",
                            style: TextStyle(fontSize: 16),
                          )),
                      Container(
                        height: 130,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            hintText: 'Masukan Sunmary',
                            hintStyle:
                                TextStyle(overflow: TextOverflow.ellipsis),
                            isDense: true,
                            contentPadding: EdgeInsets.zero,
                            border: InputBorder.none,
                          ),
                          style: const TextStyle(color: Colors.black),
                          // onChanged: (value) =>
                          //     faqSearchQuestionController.keyword.value = value,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(0),
                margin: const EdgeInsets.only(left: 10),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 1.4 -
                    MediaQuery.of(context).size.width,
                child: Stack(
                  children: [
                    Observer(
                      builder: (context) => Positioned(
                          top: 10,
                          child: Container(
                              padding: const EdgeInsets.all(0),
                              width: 150,
                              decoration: BoxDecoration(
                                color: change.value != true
                                    ? Colors.white
                                    : Colors.blue,
                                border: Border.all(
                                  width: 1,
                                  color: Colors.blue[50]!,
                                ),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color.fromRGBO(
                                          0, 0, 0, 0.07999999821186066),
                                      offset: Offset(2, 2),
                                      blurRadius: 3)
                                ],
                                borderRadius: BorderRadius.circular(
                                  25,
                                ),
                              ),
                              child: TextButton(
                                onPressed: () {
                                  change.value = !change.value;
                                },
                                child: Text("Action",
                                    style: TextStyle(
                                        color: change.value != true
                                            ? Colors.blue
                                            : Colors.white)),
                              ))),
                    ),
                    Observer(
                      builder: (context) => Positioned(
                          left: 210,
                          top: 10,
                          child: Container(
                              padding: const EdgeInsets.all(0),
                              width: 150,
                              decoration: BoxDecoration(
                                color: change.value2 != true
                                    ? Colors.white
                                    : Colors.blue,
                                border: Border.all(
                                  width: 1,
                                  color: Colors.blue[50]!,
                                ),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color.fromRGBO(
                                          0, 0, 0, 0.07999999821186066),
                                      offset: Offset(2, 2),
                                      blurRadius: 3)
                                ],
                                borderRadius: BorderRadius.circular(
                                  25,
                                ),
                              ),
                              child: TextButton(
                                onPressed: () {
                                  change.value2 = !change.value2;
                                },
                                child: Text(
                                  "Animation",
                                  style: TextStyle(
                                      color: change.value2 != true
                                          ? Colors.blue
                                          : Colors.white),
                                ),
                              ))),
                    ),
                    Observer(
                      builder: (context) => Positioned(
                          bottom: 50,
                          child: Container(
                              padding: const EdgeInsets.all(0),
                              width: 150,
                              decoration: BoxDecoration(
                                color: change.value3 != true
                                    ? Colors.white
                                    : Colors.blue,
                                border: Border.all(
                                  width: 1,
                                  color: Colors.blue[50]!,
                                ),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color.fromRGBO(
                                          0, 0, 0, 0.07999999821186066),
                                      offset: Offset(2, 2),
                                      blurRadius: 3)
                                ],
                                borderRadius: BorderRadius.circular(
                                  25,
                                ),
                              ),
                              child: TextButton(
                                onPressed: () {
                                  change.value3 = !change.value3;
                                },
                                child: Text("Drama",
                                    style: TextStyle(
                                        color: change.value3 != true
                                            ? Colors.blue
                                            : Colors.white)),
                              ))),
                    ),
                    Observer(
                      builder: (context) => Positioned(
                          left: 210,
                          bottom: 50,
                          child: Container(
                              padding: const EdgeInsets.all(0),
                              width: 150,
                              decoration: BoxDecoration(
                                color: change.value4 != true
                                    ? Colors.white
                                    : Colors.blue,
                                border: Border.all(
                                  width: 1,
                                  color: Colors.blue[50]!,
                                ),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color.fromRGBO(
                                          0, 0, 0, 0.07999999821186066),
                                      offset: Offset(2, 2),
                                      blurRadius: 3)
                                ],
                                borderRadius: BorderRadius.circular(
                                  25,
                                ),
                              ),
                              child: TextButton(
                                onPressed: () {
                                  change.value4 = !change.value4;
                                },
                                child: Text("Sci-Fi",
                                    style: TextStyle(
                                        color: change.value4 != true
                                            ? Colors.blue
                                            : Colors.white)),
                              ))),
                    ),
                  ],
                ),
              ),
            ],
          ))),
        ),
      ),
    );
  }
}
