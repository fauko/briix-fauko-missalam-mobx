import 'package:flutter/material.dart';
import 'package:managing_movies/screens/crud/add.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text("Movies Collection"),
        ),
        backgroundColor: Colors.blueAccent,
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Expanded(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      width: 1,
                      color: Colors.blue[50]!,
                    ),
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.07999999821186066),
                          offset: Offset(2, 2),
                          blurRadius: 3)
                    ],
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: const TextField(
                            decoration: InputDecoration(
                              hintText: 'Masukan pertanyaan kamu di sini',
                              hintStyle:
                                  TextStyle(overflow: TextOverflow.ellipsis),
                              isDense: true,
                              contentPadding: EdgeInsets.zero,
                              border: InputBorder.none,
                            ),
                            style: TextStyle(color: Colors.black),
                            // onChanged: (value) =>
                            //     faqSearchQuestionController.keyword.value = value,
                          ),
                        ),
                      ),
                      // Container(
                      //   padding: const EdgeInsets.symmetric(
                      //       horizontal: 15, vertical: 10),
                      //   decoration: const BoxDecoration(
                      //     color: Colors.blueAccent,
                      //     borderRadius: BorderRadius.only(
                      //         topRight: Radius.circular(10),
                      //         bottomRight: Radius.circular(10)),
                      //   ),
                      //   child: const Icon(
                      //     Icons.search,
                      //     size: 25,
                      //     color: Colors.white,
                      //   ),
                      // ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).size.width -
                      100,
                  width: MediaQuery.of(context).size.width - 90,
                  decoration: BoxDecoration(
                    //color: Colors.white12,
                    border: Border.all(
                      width: 1,
                      color: Colors.blue[50]!,
                    ),
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.07999999821186066),
                          offset: Offset(2, 2),
                          blurRadius: 1)
                    ],
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              height: 170,
                              width: MediaQuery.of(context).size.width - 96,
                              decoration: BoxDecoration(
                                // color: Colors.black12,
                                border: Border.all(
                                  width: 1,
                                  color: Colors.blue[50]!,
                                ),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color.fromRGBO(
                                          0, 0, 0, 0.07999999821186066),
                                      offset: Offset(2, 2),
                                      blurRadius: 3)
                                ],
                                borderRadius: BorderRadius.circular(
                                  10,
                                ),
                              ),
                              child: Image.network(
                                "https://img.okezone.com/content/2022/05/18/206/2596257/meriah-seluruh-bintang-power-rangers-90-an-segera-reuni-8B7PNdjTpQ.jpg",
                                fit: BoxFit.cover,
                              )),
                        ],
                      ),
                      Container(
                          padding: const EdgeInsets.all(7),
                          margin: const EdgeInsets.all(7),
                          height: MediaQuery.of(context).size.width - 250,
                          width: MediaQuery.of(context).size.width - 110,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              width: 1,
                              color: Colors.blue[50]!,
                            ),
                            boxShadow: const [
                              BoxShadow(
                                  color: Color.fromRGBO(
                                      0, 0, 0, 0.07999999821186066),
                                  offset: Offset(2, 2),
                                  blurRadius: 3)
                            ],
                            borderRadius: BorderRadius.circular(
                              5,
                            ),
                          ),
                          child: Stack(
                            children: const [
                              Text(
                                "Power Ranger",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                              ),
                              Positioned(
                                bottom: 70,
                                child: Text(
                                  "Dean Israelite",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                              ),
                              Positioned(
                                bottom: 10,
                                right: 10,
                                child: Text(
                                  "Action/Sci-Fi",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).size.width -
                      100,
                  width: MediaQuery.of(context).size.width - 90,
                  decoration: BoxDecoration(
                    //color: Colors.white12,
                    border: Border.all(
                      width: 1,
                      color: Colors.blue[50]!,
                    ),
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.07999999821186066),
                          offset: Offset(2, 2),
                          blurRadius: 1)
                    ],
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              height: 170,
                              width: MediaQuery.of(context).size.width - 96,
                              decoration: BoxDecoration(
                                // color: Colors.black12,
                                border: Border.all(
                                  width: 1,
                                  color: Colors.blue[50]!,
                                ),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color.fromRGBO(
                                          0, 0, 0, 0.07999999821186066),
                                      offset: Offset(2, 2),
                                      blurRadius: 3)
                                ],
                                borderRadius: BorderRadius.circular(
                                  10,
                                ),
                              ),
                              child: Image.network(
                                "https://img.okezone.com/content/2022/05/18/206/2596257/meriah-seluruh-bintang-power-rangers-90-an-segera-reuni-8B7PNdjTpQ.jpg",
                                fit: BoxFit.cover,
                              )),
                        ],
                      ),
                      Container(
                          padding: const EdgeInsets.all(7),
                          margin: const EdgeInsets.all(7),
                          height: MediaQuery.of(context).size.width - 250,
                          width: MediaQuery.of(context).size.width - 110,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              width: 1,
                              color: Colors.blue[50]!,
                            ),
                            boxShadow: const [
                              BoxShadow(
                                  color: Color.fromRGBO(
                                      0, 0, 0, 0.07999999821186066),
                                  offset: Offset(2, 2),
                                  blurRadius: 3)
                            ],
                            borderRadius: BorderRadius.circular(
                              5,
                            ),
                          ),
                          child: Stack(
                            children: const [
                              Text(
                                "Power Ranger",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                              ),
                              Positioned(
                                bottom: 70,
                                child: Text(
                                  "Dean Israelite",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                              ),
                              Positioned(
                                bottom: 10,
                                right: 10,
                                child: Text(
                                  "Action/Sci-Fi",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 40,
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const AddMovies()),
          );
        },
        tooltip: 'Add Movies',
        child: const Icon(Icons.add),
      ),
    );
  }
}
